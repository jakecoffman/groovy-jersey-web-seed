package com.jakecoffman.seed

import com.sun.jersey.spi.resource.Singleton
import groovy.transform.CompileStatic

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces

@Singleton
@Path("/")
class ExampleResource {
    @GET
    @Produces("application/json")
    @CompileStatic
    static Get() {
        [hello: 'groovy!']
    }
}
