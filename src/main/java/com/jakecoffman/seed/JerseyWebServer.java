package com.jakecoffman.seed;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.glassfish.grizzly.http.server.HttpServer;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class JerseyWebServer {

    private final int port;

    public JerseyWebServer(final int port) {
        this.port = port;
    }

    public void run() throws Exception {
        URI baseUri = getBaseUrl(port);
        HttpServer server = GrizzlyServerFactory.createHttpServer(baseUri);

        try {
            server.start();

            System.err.print("Server started.\n");
            synchronized (JerseyWebServer.class) {
                JerseyWebServer.class.wait();
            }
        } finally {
            server.stop();
        }
    }

    private static URI getBaseUrl(final int port) {
        return UriBuilder.fromUri("http://0.0.0.0/").port(port).build();
    }

    public static void main(final String[] args) throws Exception {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = parser.parse(options(), args);

        int port = Integer.parseInt(cmd.getOptionValue("port", "8080"));

        new JerseyWebServer(port).run();
    }

    private static Options options() {
        Options options = new Options();
        options.addOption("port", true, "server port");
        return options;
    }
}
